[![Build Status](https://travis-ci.org/gustavo-gomez/inclusixn-bot.svg?branch=master)](https://travis-ci.org/gustavo-gomez/inclusixn-bot)

A Telegram bot that echoes a text and replaces "o" by "x", etc.
